#!/usr/bin/env ruby

require 'set'

EXCLUDED_FILES = Set.new %w[
  install.rb
  README.md
]

user_dir = File.expand_path '~/'

if !File.exist?("#{user_dir}/.oh-my-zsh")
  puts "Installing oh-my-zsh..."
  `sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"`
  File.delete "#{user_dir}/.zshrc"
end

Dir.entries('.').each do |file|
  next if EXCLUDED_FILES.include?(file) || file.start_with?('.')

  symlinked_path = File.expand_path "~/.#{file}"
  if File.exist?(symlinked_path)
    puts "File already exists: #{symlinked_path}"
    next
  elsif File.symlink?(symlinked_path)
    puts "Symlink already exists: #{symlinked_path}"
    next
  end

  current_path = File.expand_path(file)
  puts "Symlinking #{current_path} to #{symlinked_path}..."
  File.symlink current_path, symlinked_path
end
