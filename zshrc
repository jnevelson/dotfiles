ZSH=$HOME/.oh-my-zsh
ZSH_THEME="bira"
plugins=(ruby osx rails gem brew redis-cli)

# git aliases
alias gad="git add"
alias gst="git status"
alias gpl="git pull"
alias gpu="git push"
alias gs="git stash"
alias gsp="git stash pop"
alias gd="git diff"
alias gdc="git diff --cached"
alias gf='git fetch'
alias gm='git merge'
alias gco='git checkout'
alias gl='git log'
alias gca='gad . ; git commit --amend'
alias gsh='git show'

source $ZSH/oh-my-zsh.sh
unsetopt correct_all # disable auto-correct

if [ -f ~/.env ]; then
  . ~/.env
fi

if [ -f ~/.aliases ]; then
  . ~/.aliases
fi

bindkey -e
export KEYTIMEOUT=1

# Speed up git completion
# http://talkings.org/post/5236392664/zsh-and-slow-git-completion
__git_files () {
  _wanted files expl 'local files' _files
}

# pip zsh completion start
function _pip_completion {
  local words cword
  read -Ac words
  read -cn cword
  reply=( $( COMP_WORDS="$words[*]" \
             COMP_CWORD=$(( cword-1 )) \
             PIP_AUTO_COMPLETE=1 $words[1] ) )
}
compctl -K _pip_completion pip
# pip zsh completion end

export PATH=$GOPATH/bin:$PATH
export EDITOR=vi

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting
