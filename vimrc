syntax on

set clipboard=unnamed
set nocompatible
set t_Co=256
set number
set history=1000
set undolevels=1000
set selectmode=mouse
set tabstop=2
set shiftwidth=2
set softtabstop=2
set smarttab
set autoindent
set smartindent
set expandtab
set showmatch "matching parenthesis
set ignorecase "ignore case on search
set smartcase
set hidden "hides buffers instead of closing them
set nowrap
set incsearch "shows search results as you type
set hlsearch
set showcmd "show commands in bottom right corner
set encoding=utf-8
set wildignore+=*/bundle,*/.git/*,*/.hg/*,*/.svn/*,*/.yardoc/*,*.exe,*.so,*.dat
set autoread "automatically reload file from disk if it has changed
set gfn=Monaco:h14

" don't need backup files
set nobackup
set noswapfile

" List Long for tab completion
set wildmenu
set wildmode=list:longest,full

" Folding
set nofoldenable
set foldmethod=marker

" Window Splitting
set splitbelow

" Powerline
set laststatus=2

set mouse=a

set guioptions-=L
set guioptions-=r

filetype off

colorscheme molokai

set rtp+=~/.vim/bundle/Vundle.vim/
call vundle#begin()
Plugin 'gmarik/vundle.vim'
Plugin 'kien/ctrlp.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-endwise'
Plugin 'tpope/vim-sensible'
Plugin 'tpope/vim-markdown'
Plugin 'tpope/vim-rbenv'
Plugin 'tpope/vim-eunuch'
Plugin 'tpope/vim-rails'
Plugin 'Lokaltog/vim-powerline'
Plugin 'tomtom/tcomment_vim'
Plugin 'rking/ag.vim'
Plugin 'fatih/vim-go'
Plugin 'editorconfig/editorconfig-vim'
Plugin 'elzr/vim-json'
Plugin 'skalnik/vim-vroom'
Plugin 'suan/vim-instant-markdown'
Plugin 'airblade/vim-gitgutter'
Plugin 'derekwyatt/vim-scala'
Plugin 'kchmck/vim-coffee-script'
call vundle#end()

filetype plugin indent on
filetype on

map q <Nop>

au BufRead,BufNewFile *.go set filetype=go
au BufNewFile,BufRead *.as set filetype=javascript
au BufNewFile,BufRead *.scala set filetype=scala
autocmd BufNewFile,BufReadPost *.md set filetype=markdown

let g:NERDCreateDefaultMappings = 0
let g:NERDSpaceDelims = 1
let g:NERDShutUp = 1
let g:NERDTreeHijackNetrw = 0
let g:NERDChristmasTree = 1
let g:NERDTreeWinPos = "left"
let g:NERDTreeCaseSensitiveSort = 1
let g:NERDTreeIgnore = ['\.vim$', '\-$','\.git']

map ff :NERDTreeTabsToggle<CR>
map <space> za

let g:rubycomplete_rails = 1

let g:vroom_use_spring = 1

let g:ackprg = 'ag --nogroup --nocolor --column'

let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 2
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files']
let g:ctrlp_follow_symlinks = 2
set runtimepath^=~/.vim/bundle/ctrlp.vim

map ; :
map <D-/> :TComment<cr>
vmap <D-/> :TComment<cr>gv

nmap <D-]> >>
nmap <D-[> <<
vmap <D-[> <gv
vmap <D-]> >gv nmap <D-]> >>

" go stuff
au FileType go nmap <leader>r <Plug>(go-run)
au FileType go nmap <leader>b <Plug>(go-build)
au FileType go nmap <leader>t <Plug>(go-test)
au FileType go nmap <Leader>i <Plug>(go-info)
au FileType go nmap <Leader>ds <Plug>(go-def-split)

let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
